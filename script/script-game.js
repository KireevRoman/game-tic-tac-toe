const btnsStart = document.querySelectorAll('#game__start .button__start');
const btnEnd = document.querySelector('#game__process .button__end');
const btnRestart = document.querySelector('#game__process .button__restart');
const scenesGame = document.querySelectorAll('#maingame .content');
const steps = document.querySelectorAll('#game__process .table_cell');
const gameMessage = document.querySelector('#game__process .game__message');
const pointer_events = document.querySelector('#game__process .game__table');
const btnsGames = document.querySelector('#game__process .game__button');
const winCombinations = [[1,2,3], [4,5,6], [7,8,9], [1,4,7], [2,5,8,], [3,6,9], [1,5,9], [3,5,7]];
let total_steps = [];
let myMove = true;
let number_scene = 0;
let player;
let computer;
let interval;

function propertyСhange(){
	for (let i=0; i < steps.length; i++){
		total_steps[i] = i;
		steps[i].textContent = '';
		steps[i].style.color = '#fff';
    }
}

function searchResult(value){
	for(var i=0; i < winCombinations.length; i++){
		h = 0;
		for (var j=0; j < winCombinations[i].length; j++){
			let index = winCombinations[i][j];
			if(steps[index-1].textContent == value){
				h++;
				if (h === 2){
					console.log('подозрительно');

					for (let k=0; k < winCombinations[i].length; k++){
						index = winCombinations[i][k];
						if(steps[index-1].textContent == '')
						return (index - 1);
					}


				}
			}
		}
	}
	return -1;
}

function moveResult(value){
	for(var i=0; i < winCombinations.length; i++){
		h = 0;
		for (var j=0; j < winCombinations[i].length; j++){
			let index = winCombinations[i][j];
			if(steps[index-1].textContent == value){
				h++;
				if (h === 3){
					for (let k=0; k < winCombinations[i].length; k++){
						index = winCombinations[i][k];
						steps[index-1].style.color = '#f7b94d';
					}
					console.log('кто то победил');
					return 1;
				}
			}
		}
	}
	console.log('играем дальше');
	return 0;
}


function GameStart() {
    scenesGame[number_scene].style.display = 'none';
    number_scene++;
    scenesGame[number_scene].style.display = 'block';

	player = this.textContent;
    mainGame();
}

function GameRestart() {
	clearTimeout(Interval);
	mainGame();
}

function GameMenu(){
	scenesGame[number_scene].style.display = 'none';
    number_scene--;
    scenesGame[number_scene].style.display = 'block';
}

function GameEnd(mess) {
	clearTimeout(Interval);
	total_steps = [];
	gameMessage.textContent = mess;
	btnsGames.classList.remove('hide');
}

function randomInteger(min, max) {
	let rand = min + Math.random() * (max - min);
	return Math.floor(rand);
}

function preload(){
	btnEnd.onclick = GameMenu;
	btnRestart.onclick = GameRestart;

	for (btn_start of btnsStart) {
		btn_start.addEventListener('click', GameStart);
	}

	for (step of steps) {
		console.log(step.textContent);
		step.addEventListener('click', function(){

			if (this.textContent === '') {
				this.textContent = player;
				pointer_events.classList.add('disabled__pointer_events');

				if (moveResult(player) === 1) {
					GameEnd('Вы победили!');
				} else{
					gameMessage.textContent = 'Ход компьютера!';
					for(let i=0; i < total_steps.length; i++){
						if (this.id == total_steps[i]){
							total_steps.splice(i, 1);
							break;
						}
					}
					myMove = !myMove;
					console.log(total_steps);
				}

			} else{
				console.log('недопустимый ход');
			}

		});
	}
}

function mainGame(){

	if (player === '?') {
		let tmp = (Math.random()>0.5)? 1 : 0;
		player = (tmp == 1) ? 'X' : 'O';
	}

	if (player === 'X') {
		computer = 'O';
		myMove = true;
		gameMessage.textContent = 'Ваш ход!';
		pointer_events.classList.remove('disabled__pointer_events');
	}else{
		computer = 'X';
		myMove = false;
		gameMessage.textContent = 'Ход компьютера!';
		pointer_events.classList.add('disabled__pointer_events');
	}



	btnsGames.classList.add('hide');
	propertyСhange();

	Interval = setInterval(function(){

		if (total_steps.length === 0){
			GameEnd('Ничья');
			return;
		}

		if (!myMove){
			setTimeout(function(){
				let a;
				let compMove = searchResult(computer);
				if (compMove === -1){
					compMove = searchResult(player);
					if (compMove === -1){
						a = randomInteger(0, total_steps.length);
						compMove = total_steps[a];
					}
				}

				steps[compMove].textContent = computer;

				if (moveResult(computer) === 1){
					GameEnd('Победил компьютер');
				}else{
					gameMessage.textContent = 'Ваш ход!';
					pointer_events.classList.remove('disabled__pointer_events');
					for(let i=0; i < total_steps.length; i++){
						if (compMove == total_steps[i]){
							total_steps.splice(i, 1);
							break;
						}
					}
				}

			}, 250);
			myMove = !myMove;
		};

	console.log(myMove);
	}, 500);
}

preload();